#include "texture.h"
#include "game/game.h"

namespace endless
{
	namespace texture
	{
		Texture2D backgroundGameplay;
		Texture2D backgroundMenu;
		Texture2D backgroundGameover;

		Texture2D floor;
		Texture2D parallax1;
		Texture2D parallax2;
		Texture2D parallax3;
		Texture2D parallax4;
		
		Texture2D playerIdle;
		Texture2D playerRun;
		Texture2D playerJump;
		Texture2D playerHitted;
		Texture2D playerDead;
		Texture2D playerAttack;

		Texture2D heartHUD;
		Texture2D potion;

		Texture2D enemyRun;
		Texture2D enemyDead;

		Texture2D spikes;
		Texture2D rock;

		Texture2D gemBlue;
		Texture2D gemYellow;
		Texture2D gemGreen;
		Texture2D gemOrange;

		void initMenu()
		{
			backgroundMenu = LoadTexture("res/assets/textures/background_menu.png");
			setSizesMenu();
		}

		void setSizesMenu()
		{
			backgroundMenu.width = game::screenWidth;
			backgroundMenu.height = game::screenHeight;
		}

		void deInitMenu()
		{
			UnloadTexture(backgroundMenu);
		}

		void initGameover()
		{
			backgroundGameover = LoadTexture("res/assets/textures/background_gameover.png");

			backgroundGameover.width = game::screenWidth;
			backgroundGameover.height = game::screenHeight;
		}

		void deInitGameover()
		{
			UnloadTexture(backgroundGameover);
		}

		void initGameplay()
		{
			backgroundGameplay = LoadTexture("res/assets/textures/background_gameplay.png");

			floor = LoadTexture("res/assets/textures/floor.png");
			parallax1 = LoadTexture("res/assets/textures/parallax1.png");
			parallax2 = LoadTexture("res/assets/textures/parallax2.png");
			parallax3 = LoadTexture("res/assets/textures/parallax3.png");
			parallax4 = LoadTexture("res/assets/textures/parallax4.png");

			playerIdle = LoadTexture("res/assets/textures/player_idle.png");
			playerRun = LoadTexture("res/assets/textures/player_run.png");
			playerJump = LoadTexture("res/assets/textures/player_jump.png");
			playerHitted = LoadTexture("res/assets/textures/player_hitted.png");
			playerDead = LoadTexture("res/assets/textures/player_dead.png");
			playerAttack = LoadTexture("res/assets/textures/player_attack.png");

			heartHUD = LoadTexture("res/assets/textures/heart_hud.png");
			potion = LoadTexture("res/assets/textures/potion.png");

			enemyRun = LoadTexture("res/assets/textures/enemy_run.png");
			enemyDead = LoadTexture("res/assets/textures/enemy_dead.png");

			spikes = LoadTexture("res/assets/textures/spikes.png");
			rock = LoadTexture("res/assets/textures/rock.png");

			gemBlue = LoadTexture("res/assets/textures/gem_blue.png");
			gemYellow = LoadTexture("res/assets/textures/gem_yellow.png");
			gemGreen = LoadTexture("res/assets/textures/gem_green.png");
			gemOrange = LoadTexture("res/assets/textures/gem_orange.png");

			setTextureSizes();
		}

		void deInitGameplay()
		{
			UnloadTexture(backgroundGameplay);

			UnloadTexture(floor);
			UnloadTexture(parallax1);
			UnloadTexture(parallax2);
			UnloadTexture(parallax3);
			UnloadTexture(parallax4);

			UnloadTexture(playerIdle);
			UnloadTexture(playerRun);
			UnloadTexture(playerJump);
			UnloadTexture(playerHitted);
			UnloadTexture(playerDead);
			UnloadTexture(playerAttack);

			UnloadTexture(heartHUD);
			UnloadTexture(potion);

			UnloadTexture(enemyRun);
			UnloadTexture(enemyDead);

			UnloadTexture(spikes);
			UnloadTexture(rock);

			UnloadTexture(gemBlue);
			UnloadTexture(gemYellow);
			UnloadTexture(gemGreen);
			UnloadTexture(gemOrange);
		}

		void setTextureSizes()
		{
			backgroundGameplay.width = game::screenWidth;
			backgroundGameplay.height = game::screenHeight;

			floor.width = game::screenWidth;
			floor.height = game::screenHeight / 8;

			int bgHeight = static_cast<int>(gameplay::floorPositionY[0] + floor.height + 20);

			parallax1.width = game::screenWidth;
			parallax1.height = bgHeight;

			parallax2.width = game::screenWidth;
			parallax2.height = bgHeight;

			parallax3.width = game::screenWidth;
			parallax3.height = bgHeight;

			parallax4.width = game::screenWidth;
			parallax4.height = bgHeight;

			//Player
			playerIdle.width = static_cast<int>(player::playerWidth * 6);
			playerIdle.height = static_cast<int>(player::playerHeight * 2);

			playerRun.width = static_cast<int>(player::playerWidth * 6);
			playerRun.height = static_cast<int>(player::playerHeight * 3);

			playerJump.width = static_cast<int>(player::playerWidth * 6);
			playerJump.height = static_cast<int>(player::playerHeight);

			playerHitted.width = static_cast<int>(player::playerWidth * 6);
			playerHitted.height = static_cast<int>(player::playerHeight * 2);

			playerDead.width = static_cast<int>(player::playerWidth * 6);
			playerDead.height = static_cast<int>(player::playerHeight * 3);

			playerAttack.width = static_cast<int>(player::playerWidth * 6);
			playerAttack.height = static_cast<int>(player::playerHeight * 2);

			//Heart
			heartHUD.width = static_cast<int>(fonts::optionFontSize);
			heartHUD.height = static_cast<int>(fonts::optionFontSize);

			potion.width = static_cast<int>(potion::potionWidth);
			potion.height = static_cast<int>(potion::potionHeight);

			//Gem
			int gWidth = static_cast<int>(gem::gemWidth);
			int gHeight = static_cast<int>(gem::gemHeight);

			gemBlue.width = gWidth;
			gemBlue.height = gHeight;

			gemYellow.width = gWidth;
			gemYellow.height = gHeight;

			gemGreen.width = gWidth;
			gemGreen.height = gHeight;

			gemOrange.width = gWidth;
			gemOrange.height = gHeight;

			//Trap
			spikes.width = static_cast<int>(trap::spikesWidth);
			spikes.height = static_cast<int>(trap::spikesHeight);

			rock.width = static_cast<int>(trap::rockWidth);
			rock.height = static_cast<int>(trap::rockHeight);

			//Enemy
			enemyRun.width = static_cast<int>(enemy::enemyWidth * 6);
			enemyRun.height = static_cast<int>(enemy::enemyHeight * 2);

			enemyDead.width = static_cast<int>(enemy::enemyWidth * 6);
			enemyDead.height = static_cast<int>(enemy::enemyHeight * 3);
		}
	}
}