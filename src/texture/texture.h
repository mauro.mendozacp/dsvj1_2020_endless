#ifndef TEXTURE_H
#define TEXTURE_H

#include "raylib.h"

namespace endless
{
	namespace texture
	{
		extern Texture2D backgroundGameplay;
		extern Texture2D backgroundMenu;
		extern Texture2D backgroundGameover;

		extern Texture2D floor;
		extern Texture2D parallax1;
		extern Texture2D parallax2;
		extern Texture2D parallax3;
		extern Texture2D parallax4;

		extern Texture2D playerIdle;
		extern Texture2D playerRun;
		extern Texture2D playerJump;
		extern Texture2D playerHitted;
		extern Texture2D playerDead;
		extern Texture2D playerAttack;

		extern Texture2D heartHUD;
		extern Texture2D potion;

		extern Texture2D enemyRun;
		extern Texture2D enemyDead;

		extern Texture2D spikes;
		extern Texture2D rock;

		extern Texture2D gemBlue;
		extern Texture2D gemYellow;
		extern Texture2D gemGreen;
		extern Texture2D gemOrange;

		void initMenu();
		void setSizesMenu();
		void deInitMenu();
		void initGameover();
		void deInitGameover();
		void initGameplay();
		void deInitGameplay();
		void setTextureSizes();
	}
}

#endif // !TEXTURE_H