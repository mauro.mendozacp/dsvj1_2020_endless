#include "input.h"

namespace endless
{
	namespace input
	{
		int moveUpPlayer = KEY_UP;
		int moveDownPlayer = KEY_DOWN;
		int jumpPlayer = KEY_SPACE;
		int attackPlayer = KEY_A;
		int pauseGame = KEY_P;

		bool checkMoveUpOption()
		{
			if (IsKeyPressed(KEY_UP))
			{
				return true;
			}
			return false;
		}

		bool checkMoveDownOption()
		{
			if (IsKeyPressed(KEY_DOWN))
			{
				return true;
			}
			return false;
		}

		bool checkMoveLeftOption()
		{
			if (IsKeyPressed(KEY_LEFT))
			{
				return true;
			}
			return false;
		}
		bool checkMoveRightOption()
		{
			if (IsKeyPressed(KEY_RIGHT))
			{
				return true;
			}
			return false;
		}

		bool checkAcceptOption()
		{
			if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER))
			{
				return true;
			}
			return false;
		}

		bool checkStartGame()
		{
			if (IsKeyPressed(KEY_SPACE))
			{
				return true;
			}
			return false;
		}

		bool checkMoveUpPlayer()
		{
			if (IsKeyPressed(moveUpPlayer))
			{
				return true;
			}
			return false;
		}

		bool checkMoveDownPlayer()
		{
			if (IsKeyPressed(moveDownPlayer))
			{
				return true;
			}
			return false;
		}

		bool checkJump()
		{
			if (IsKeyPressed(jumpPlayer))
			{
				return true;
			}
			return false;
		}

		bool checkAttack()
		{
			if (IsKeyPressed(attackPlayer))
			{
				return true;
			}
			return false;
		}

		bool checkPauseGame()
		{
			if (IsKeyPressed(pauseGame))
			{
				return true;
			}
			return false;
		}

		bool checkMoveUpDownOption()
		{
			if (checkMoveUpOption() || checkMoveDownOption())
			{
				return true;
			}
			return false;
		}
		bool checkMoveLeftRightOption()
		{
			if (checkMoveLeftOption() || checkMoveRightOption())
			{
				return true;
			}
			return false;
		}

		bool checkMoveUpDownPlayer()
		{
			if (checkMoveUpPlayer() || checkMoveDownPlayer())
			{
				return true;
			}
			return false;
		}

		int GetInputChange()
		{
			int inputKey = GetKeyPressed();

			if ((inputKey > 32) && (inputKey <= 126))
			{
				if (inputKey >= 97 && inputKey <= 122)
				{
					inputKey -= 32;
				}
				return inputKey;
			}
			if (IsKeyPressed(KEY_UP))
			{
				return KEY_UP;
			}
			if (IsKeyPressed(KEY_DOWN))
			{
				return KEY_DOWN;
			}
			if (IsKeyPressed(KEY_LEFT))
			{
				return KEY_LEFT;
			}
			if (IsKeyPressed(KEY_RIGHT))
			{
				return KEY_RIGHT;
			}
			if (IsKeyPressed(KEY_SPACE))
			{
				return KEY_SPACE;
			}

			return 0;
		}

		const char* GetInputText(int iKey)
		{
			if ((iKey > 32) && (iKey <= 126))
			{
				return TextFormat("%c", iKey);
			}
			if (iKey == KEY_UP)
			{
				return "ARROW UP";
			}
			if (iKey == KEY_DOWN)
			{
				return "ARROW DOWN";
			}
			if (iKey == KEY_LEFT)
			{
				return "ARROW LEFT";
			}
			if (iKey == KEY_RIGHT)
			{
				return "ARROW RIGHT";
			}
			if (iKey == KEY_SPACE)
			{
				return "SPACE";
			}

			return " ";
		}
	}
}