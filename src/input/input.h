#pragma once
#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace endless
{
	namespace input
	{
		extern int moveUpPlayer;
		extern int moveDownPlayer;
		extern int jumpPlayer;
		extern int attackPlayer;
		extern int pauseGame;

		bool checkMoveUpOption();
		bool checkMoveDownOption();
		bool checkMoveLeftOption();
		bool checkMoveRightOption();
		bool checkAcceptOption();
		bool checkStartGame();
		bool checkMoveUpPlayer();
		bool checkMoveDownPlayer();
		bool checkJump();
		bool checkAttack();
		bool checkPauseGame();
		bool checkMoveUpDownOption();
		bool checkMoveLeftRightOption();
		bool checkMoveUpDownPlayer();

		int GetInputChange();
		const char* GetInputText(int iKey);
	}
}

#endif // !INPUT_H
