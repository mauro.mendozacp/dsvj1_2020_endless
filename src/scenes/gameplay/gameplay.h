#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "entities/player/player.h"
#include "entities/pickup/pickup.h"
#include "entities/gem/gem.h"
#include "entities/potion/potion.h"
#include "entities/obstacle/obstacle.h"
#include "entities/trap/trap.h"
#include "entities/enemy/enemy.h"
#include "entities/background/background.h"

namespace endless
{
	namespace gameplay
	{
		extern player::Player* player;

		extern const int pickupLenght;
		extern pickup::Pickup* pickups[];

		extern const int obstacleLenght;
		extern obstacle::Obstacle* obstacles[];

		extern const int parallaxLenght;
		extern background::Background* parallax[];
		extern const int floorLenght;
		extern background::Background* floor[];
		extern float floorPositionY[];
		extern float floorHeight;

		extern float speedX;
		extern float timer;

		void init();
		void update();
		void draw();
		void deInit();

		void restartGame();
	}
}

#endif // !GAMEPLAY_H