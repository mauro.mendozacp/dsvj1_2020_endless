#include "gameplay.h"
#include "game/game.h"
#include <iostream>

namespace endless
{
	namespace gameplay
	{
		player::Player* player;

		const int pickupLenght = 5;
		pickup::Pickup* pickups[pickupLenght];

		const int obstacleLenght = 5;
		obstacle::Obstacle* obstacles[obstacleLenght];

		const int parallaxLenght = 4;
		background::Background* parallax[parallaxLenght];
		const int floorLenght = 3;
		background::Background* floor[floorLenght];
		float floorPositionY[floorLenght];
		float floorHeight;

		bool startGame;
		float speedX = 400.0f;
		float timer;

		const float cooldownPickupBase = 2.0f;
		const float cooldownObstacleBase = 3.0f;
		const float cooldownPickupMin = 1.0f;
		const float cooldownObstacleMin = 1.0f;
		float cooldownPickup;
		float cooldownObstacle;
		float timerPickup;
		float timerObstacle;

		static void showHUD()
		{
			Font option = fonts::option;
			float optionFontSize = static_cast<float>(fonts::optionFontSize);
			float optionSpacing = static_cast<float>(fonts::optionSpacing);

			//Title
			const char* text = FormatText("LIFES: ");
			int spacingBoard = 15;
			int posX = static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).x / 2) + spacingBoard;
			int posY = static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).y / 2) + spacingBoard;
			
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			posX = static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).x) + spacingBoard;
			for (int i = 0; i < player->getLifes(); i++)
			{
				DrawTexture(texture::heartHUD, (posX + (static_cast<int>(optionFontSize) * i)), spacingBoard, WHITE);
			}

			int min = static_cast<int>(timer) / 60;
			int seg = static_cast<int>(timer) % 60;
			
			if (min < 10)
			{
				if (seg < 10)
				{
					text = FormatText("TIME: 0%i:0%i", min, seg);
				}
				else
				{
					text = FormatText("TIME: 0%i:%i", min, seg);
				}
			}
			else
			{
				if (seg < 10)
				{
					text = FormatText("TIME: %i:0%i", min, seg);
				}
				else
				{
					text = FormatText("TIME: %i:%i", min, seg);
				}
			}

			posX = game::screenWidth / 2;
			posY = static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).y / 2) + spacingBoard;

			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = FormatText("SCORE: %i", player->getScore());
			posX = game::screenWidth - static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).x / 2) - spacingBoard;
			posY = static_cast<int>(MeasureTextEx(option, text, optionFontSize, optionSpacing).y / 2) + spacingBoard;

			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);
		}

		static void pauseGame()
		{
			if (input::checkPauseGame())
			{
				game::gameStatus = game::GAME_STATUS::PAUSE;
				pause::init();
			}
		}

		static void finishGame()
		{
			if (player->getLifes() <= 0)
			{
				player->updateAnimation();
				if (player->getAnimation().getIsFinish())
				{
					StopMusicStream(audio::gameplayMusic);
					game::gameStatus = game::GAME_STATUS::GAMEOVER;
					gameover::init();
				}
			}
		}

		void init()
		{
			floorHeight = static_cast<float>(game::screenHeight / 8);
			for (int i = 0; i < floorLenght; i++)
			{
				floorPositionY[i] = static_cast<float>(game::screenHeight - (floorHeight * floorLenght) + (floorHeight * i));
			}
			player::playerHeight = floorHeight * 125 / 100;
			player::playerWidth = static_cast<float>(game::screenWidth) / 10;
			trap::spikesWidth = player::playerWidth * 3 / 4;
			trap::spikesHeight = floorHeight * 3 / 8;
			trap::rockWidth = player::playerWidth;
			trap::rockHeight = floorHeight;
			enemy::enemyHeight = floorHeight * 125 / 100;
			enemy::enemyWidth = static_cast<float>(game::screenWidth) / 10;
			gem::gemWidth = static_cast<float>(game::screenWidth) / 16;
			gem::gemHeight = static_cast<float>(game::screenHeight) / 16;
			potion::potionWidth = static_cast<float>(game::screenWidth) / 16;
			potion::potionHeight = static_cast<float>(game::screenHeight) / 16;

			texture::initGameplay();
			player::init();
			pickup::init();
			obstacle::init();
			background::init();
			timer = 0.0f;
			startGame = false;
			timerPickup = 0.0f;
			timerObstacle = 0.0f;
			cooldownPickup = cooldownPickupBase;
			cooldownObstacle = cooldownObstacleBase;
		}

		void update()
		{
			if (startGame)
			{
				if (player->getLifes() > 0)
				{
					int auxFloor1 = 0;
					int auxFloor2 = 0;
					float frameTime = GetFrameTime();
					timer += frameTime;

					background::update();

					timerPickup += frameTime;
					timerObstacle += frameTime;

					if (timerPickup >= cooldownPickup)
					{
						pickup::create(GetRandomValue(0, (floorLenght - 1)));

						timerPickup = 0.0f;
						
						cooldownPickup -= frameTime;
						if (cooldownPickup < cooldownPickupMin)
						{
							cooldownPickup = cooldownPickupMin;
						}
					}

					if (timerObstacle >= cooldownObstacle)
					{
						auxFloor1 = GetRandomValue(0, (floorLenght - 1));
						obstacle::create(auxFloor1);

						if (timer > game::frames * 1)
						{
							auxFloor2 = GetRandomValue(0, (floorLenght - 1));

							if (auxFloor1 != auxFloor2)
							{
								obstacle::create(auxFloor2);
							}
						}

						timerObstacle = 0.0f;
						
						cooldownObstacle -= frameTime;
						if (cooldownObstacle < cooldownObstacleMin)
						{
							cooldownObstacle = cooldownObstacleMin;
						}
					}

					pickup::update();
					obstacle::update();

					player::update();
				}
				finishGame();
				UpdateMusicStream(audio::gameplayMusic);
			}
			else
			{
				if (input::checkStartGame())
				{
					player->changeAnimation(animation::ANIMATION_STATE::RUN);

					PlayMusicStream(audio::gameplayMusic);
					startGame = true;
				}

				player->updateAnimation();
			}

			pauseGame();
		}

		void draw()
		{
			background::draw();
			pickup::draw();
			obstacle::draw();
			player::draw();

			if (!startGame)
			{
				//Title
				const char* text = "PRESS SPACE FOR START";
				int posX = game::screenWidth / 2;
				int posY = game::screenHeight / 2;

				fonts::drawCenterOption(text, posX, posY, fonts::titleColor);
			}

			showHUD();
		}

		void deInit()
		{
			player::deInit();
			pickup::deInit();
			obstacle::deInit();
			background::deInit();
			texture::deInitGameplay();
			texture::initMenu();
		}

		void restartGame()
		{
			player::deInit();
			pickup::deInit();
			obstacle::deInit();
			background::deInit();

			player::init();
			pickup::init();
			obstacle::init();
			background::init();
			timer = 0.0f;
			startGame = false;
			timerPickup = 0.0f;
			timerObstacle = 0.0f;
			cooldownPickup = cooldownPickupBase;
			cooldownObstacle = cooldownObstacleBase;
		}
	}
}