#ifndef CONTROLS_SETTINGS_H
#define CONTROLS_SETTINGS_H

#include "raylib.h"

namespace endless
{
	namespace controls_settings
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !CONTROLS_SETTINGS_H