#include "controls_settings.h"
#include "game/game.h"

namespace endless
{
	namespace controls_settings
	{
		enum class CONTROLS_SETTINGS_MENU
		{
			MOVE_UP_PLAYER = 1,
			MOVE_DOWN_PLAYER,
			JUMP_PLAYER,
			ATTACK_PLAYER,
			PAUSE_GAME,
			GO_BACK
		};

		CONTROLS_SETTINGS_MENU controlsSettingsMenu;
		bool wait;

		static CONTROLS_SETTINGS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<CONTROLS_SETTINGS_MENU>(static_cast<int>(controlsSettingsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<CONTROLS_SETTINGS_MENU>(static_cast<int>(controlsSettingsMenu) + 1);
			}

			return controlsSettingsMenu;
		}

		static bool checkMainMenuOption(CONTROLS_SETTINGS_MENU auxOption)
		{
			switch (auxOption)
			{
			case CONTROLS_SETTINGS_MENU::MOVE_DOWN_PLAYER:
			case CONTROLS_SETTINGS_MENU::MOVE_UP_PLAYER:
			case CONTROLS_SETTINGS_MENU::JUMP_PLAYER:
			case CONTROLS_SETTINGS_MENU::ATTACK_PLAYER:
			case CONTROLS_SETTINGS_MENU::PAUSE_GAME:
			case CONTROLS_SETTINGS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption() && !wait)
			{
				CONTROLS_SETTINGS_MENU auxOption = getNewOptionMenu();

				if (checkMainMenuOption(auxOption))
				{
					controlsSettingsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void changeInput()
		{
			if (wait)
			{
				int inputKey = input::GetInputChange();

				switch (controlsSettingsMenu)
				{
				case CONTROLS_SETTINGS_MENU::MOVE_UP_PLAYER:
					input::moveUpPlayer = inputKey;
					break;
				case CONTROLS_SETTINGS_MENU::MOVE_DOWN_PLAYER:
					input::moveDownPlayer = inputKey;
					break;
				case CONTROLS_SETTINGS_MENU::JUMP_PLAYER:
					input::jumpPlayer = inputKey;
					break;
				case CONTROLS_SETTINGS_MENU::ATTACK_PLAYER:
					input::attackPlayer = inputKey;
					break;
				case CONTROLS_SETTINGS_MENU::PAUSE_GAME:
					input::pauseGame = inputKey;
					break;
				default:
					break;
				}

				if (inputKey != 0)
				{
					wait = false;
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				if (controlsSettingsMenu == CONTROLS_SETTINGS_MENU::GO_BACK)
				{
					game::changeStatus(game::GAME_STATUS::SETTINGS);
				}
				else
				{
					wait = true;
				}
			}
		}

		static Color getColorOption(CONTROLS_SETTINGS_MENU option)
		{
			if (option == controlsSettingsMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			controlsSettingsMenu = CONTROLS_SETTINGS_MENU::MOVE_UP_PLAYER;
			wait = false;
		}

		void update()
		{
			moveOption();
			acceptOption();
			changeInput();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "CONTROLS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "MOVE UP PLAYER";
			posX = game::screenWidth / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::MOVE_UP_PLAYER));

			text = "MOVE DOWN PLAYER";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::MOVE_DOWN_PLAYER));

			text = "JUMP PLAYER";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::JUMP_PLAYER));

			text = "ATTACK PLAYER";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::ATTACK_PLAYER));

			text = "PAUSE GAME";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::PAUSE_GAME));

			text = "GO BACK";
			posX = game::screenWidth / 2;
			posY = game::screenHeight * 14 / 16;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::GO_BACK));

			//Values
			text = input::GetInputText(input::moveUpPlayer);
			posX = game::screenWidth * 3 / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::MOVE_UP_PLAYER));

			text = input::GetInputText(input::moveDownPlayer);
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::MOVE_DOWN_PLAYER));

			text = input::GetInputText(input::jumpPlayer);
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::JUMP_PLAYER));

			text = input::GetInputText(input::attackPlayer);
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::ATTACK_PLAYER));

			text = input::GetInputText(input::pauseGame);
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(CONTROLS_SETTINGS_MENU::PAUSE_GAME));
		}

		void deInit()
		{
		}
	}
}