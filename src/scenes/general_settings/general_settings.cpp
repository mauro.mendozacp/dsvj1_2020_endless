#include "general_settings.h"
#include "game/game.h"

namespace endless
{
	namespace general_settings
	{
		const int resolutionLenght = 3;
		Vector2 resolutionValues[resolutionLenght] = { { 600, 400 }, { 840, 750 }, { 1240, 900 } };
		const char* resolutionsText[resolutionLenght] = { "600x400", "840x750", "1240x900" };
		int fontTitles[resolutionLenght] = { 35, 55, 75 };
		int fontOptions[resolutionLenght] = { 17, 25, 35 };
		int fontValues[resolutionLenght] = { 10, 15, 25 };

		const int volumeLenght = 6;
		float volumeValues[volumeLenght] = { 0, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f };
		float musicVolume = volumeValues[3];
		float sfxVolume = volumeValues[4];

		Vector2 resolution = resolutionValues[1];

		enum class GENERAL_SETTINGS_MENU
		{
			RESOLUTION = 1,
			MUSIC,
			SFX,
			GO_BACK
		};

		GENERAL_SETTINGS_MENU generalSettingsMenu;

		static int getNewValue(int currentValue, int minValue, int maxValue)
		{
			int auxValue = 0;

			if (input::checkMoveLeftOption())
			{
				auxValue = currentValue - 1;
				if (auxValue >= minValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}
			if (input::checkMoveRightOption())
			{
				auxValue = currentValue + 1;
				if (auxValue <= maxValue)
				{
					return auxValue;
				}
				else
				{
					return currentValue;
				}
			}

			return currentValue;
		}

		static int getIndexResolutionValue(float width, float height)
		{
			for (int i = 0; i < resolutionLenght; i++)
			{
				if (width == resolutionValues[i].x && height == resolutionValues[i].y)
				{
					return i;
				}
			}

			return -1;
		}

		static int getIndexVolumeValue(float volume)
		{
			for (int i = 0; i < volumeLenght; i++)
			{
				if (volume == volumeValues[i])
				{
					return i;
				}
			}

			return -1;
		}

		static void changeValue()
		{
			if (input::checkMoveLeftRightOption())
			{
				switch (generalSettingsMenu)
				{
				case GENERAL_SETTINGS_MENU::RESOLUTION:
					resolution = resolutionValues[getNewValue(getIndexResolutionValue(resolution.x, resolution.y), 0, (resolutionLenght - 1))];
					break;
				case GENERAL_SETTINGS_MENU::MUSIC:
					musicVolume = volumeValues[getNewValue(getIndexVolumeValue(musicVolume), 0, (volumeLenght - 1))];
					audio::setMusicVolume(musicVolume);
					break;
				case GENERAL_SETTINGS_MENU::SFX:
					sfxVolume = volumeValues[getNewValue(getIndexVolumeValue(sfxVolume), 0, (volumeLenght - 1))];
					audio::setSfxVolume(sfxVolume);
					break;
				default:
					break;
				}
			}
		}

		static GENERAL_SETTINGS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<GENERAL_SETTINGS_MENU>(static_cast<int>(generalSettingsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<GENERAL_SETTINGS_MENU>(static_cast<int>(generalSettingsMenu) + 1);
			}

			return generalSettingsMenu;
		}

		static bool checkOption(GENERAL_SETTINGS_MENU auxOption)
		{
			switch (auxOption)
			{
			case GENERAL_SETTINGS_MENU::RESOLUTION:
			case GENERAL_SETTINGS_MENU::MUSIC:
			case GENERAL_SETTINGS_MENU::SFX:
			case GENERAL_SETTINGS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				GENERAL_SETTINGS_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					generalSettingsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				if (generalSettingsMenu == GENERAL_SETTINGS_MENU::GO_BACK)
				{
					game::changeStatus(game::GAME_STATUS::SETTINGS);
				}
			}
		}

		static Color getColorOption(GENERAL_SETTINGS_MENU option)
		{
			if (option == generalSettingsMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			generalSettingsMenu = GENERAL_SETTINGS_MENU::RESOLUTION; 
			
			resolution = { static_cast<float>(game::screenWidth), static_cast<float>(game::screenHeight) };
			musicVolume = audio::musicVolume;
			sfxVolume = audio::sfxVolume;
		}

		void update()
		{
			moveOption();
			changeValue();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "GENERAL";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "RESOLUTION";
			posX = game::screenWidth / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::RESOLUTION));

			text = "MUSIC";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::MUSIC));

			text = "SFX";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::SFX));

			text = "GO BACK";
			posX = game::screenWidth / 2;
			posY = game::screenHeight * 3 / 4;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::GO_BACK));
			
			//Values
			float auxVolume = 0;

			text = resolutionsText[getIndexResolutionValue(resolution.x, resolution.y)];
			posX = game::screenWidth * 3 / 4;
			posY = game::screenHeight / 2;
			fonts::drawCenterValue(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::RESOLUTION));
			
			auxVolume = volumeValues[getIndexVolumeValue(musicVolume)] * 100;
			text = TextFormat("%.0f", auxVolume);
			posY += spacingTextY;
			fonts::drawCenterValue(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::MUSIC));

			auxVolume = volumeValues[getIndexVolumeValue(sfxVolume)] * 100;
			text = TextFormat("%.0f", auxVolume);
			posY += spacingTextY;
			fonts::drawCenterValue(text, posX, posY, getColorOption(GENERAL_SETTINGS_MENU::SFX));

			//Arrow decrease
			int font = fonts::optionFontSize;
			posX = (game::screenWidth * 3 / 4) - fonts::optionFontSize - 35;
			text = "<";

			posY = game::screenHeight / 2;
			if (getIndexResolutionValue(resolution.x, resolution.y) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}

			posY += spacingTextY;
			if (getIndexVolumeValue(musicVolume) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}

			posY += spacingTextY;
			if (getIndexVolumeValue(sfxVolume) > 0)
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}

			//Arrow increase
			posX = (game::screenWidth * 3 / 4) + fonts::optionFontSize + 35;
			text = ">";

			posY = game::screenHeight / 2;
			if (getIndexResolutionValue(resolution.x, resolution.y) < (resolutionLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}

			posY += spacingTextY;
			if (getIndexVolumeValue(musicVolume) < (volumeLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}

			posY += spacingTextY;
			if (getIndexVolumeValue(sfxVolume) < (volumeLenght - 1))
			{
				DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, fonts::optionColorNoSelected);
			}
		}

		void deInit()
		{
			if (resolution.x != static_cast<float>(game::screenWidth) || resolution.y != static_cast<float>(game::screenHeight))
			{
				game::screenWidth = static_cast<int>(resolution.x);
				game::screenHeight = static_cast<int>(resolution.y);

				fonts::titleFontSize = fontTitles[getIndexResolutionValue(resolution.x, resolution.y)];
				fonts::optionFontSize = fontOptions[getIndexResolutionValue(resolution.x, resolution.y)];
				fonts::valueFontSize = fontValues[getIndexResolutionValue(resolution.x, resolution.y)];

				SetWindowSize(game::screenWidth, game::screenHeight);
				texture::setSizesMenu();
			}
		}
	}
}