#ifndef GENERAL_H
#define GENERAL_H

#include "raylib.h"

namespace endless
{
	namespace general_settings
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GENERAL_H