#include "gameover.h"
#include "game/game.h"

namespace endless
{
	namespace gameover
	{
		enum class GAMEOVER_MENU
		{
			RESTART = 1,
			GO_BACK
		};

		GAMEOVER_MENU gameoverMenu;

		static GAMEOVER_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<GAMEOVER_MENU>(static_cast<int>(gameoverMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<GAMEOVER_MENU>(static_cast<int>(gameoverMenu) + 1);
			}

			return gameoverMenu;
		}

		static bool checkOption(GAMEOVER_MENU auxOption)
		{
			switch (auxOption)
			{
			case GAMEOVER_MENU::RESTART:
			case GAMEOVER_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				GAMEOVER_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					gameoverMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (gameoverMenu)
				{
				case GAMEOVER_MENU::RESTART:
					game::gameStatus = game::GAME_STATUS::GAMEPLAY;
					gameplay::restartGame();
					deInit();
					break;
				case GAMEOVER_MENU::GO_BACK:
					gameplay::deInit();
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(GAMEOVER_MENU option)
		{
			if (option == gameoverMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			gameoverMenu = GAMEOVER_MENU::RESTART;
			texture::initGameover();

			if (gameplay::player->getScore() > game::highScore)
			{
				SaveStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE, gameplay::player->getScore());
			}
		}

		void update()
		{
			moveOption();
			acceptOption();
		}

		void draw()
		{
			//Title
			const char* text = "GAME OVER";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Highscore
			if (gameplay::player->getScore() > game::highScore)
			{
				text = TextFormat("NEW HIGHSCORE: %i", gameplay::player->getScore());
			}
			else
			{
				text = TextFormat("YOU SCORE: %i", gameplay::player->getScore());
			}
			posY += static_cast<int>(fonts::titleFontSize + (fonts::titleFontSize / 2));
			fonts::drawCenterOption(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "RESTART";
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GAMEOVER_MENU::RESTART));

			text = "BACK TO MENU";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(GAMEOVER_MENU::GO_BACK));
		}

		void deInit()
		{
			texture::deInitGameover();
			texture::initMenu();
			game::highScore = LoadStorageValue(game::STORAGE_DATA::STORAGE_POSITION_HISCORE);
		}
	}
}