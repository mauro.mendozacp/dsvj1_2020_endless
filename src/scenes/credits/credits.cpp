#include "credits.h"
#include "game/game.h"

namespace endless
{
	namespace credits
	{
		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
			}
		}

		void init()
		{
		}

		void update()
		{
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "GAME DEVELOPER";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 8;
			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			text = "MAURO MENDOZA";
			posY += static_cast<int>(fonts::titleFontSize + (fonts::titleFontSize / 2));
			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "Textures: craftpix.net/file-licenses";
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = "Fonts: googlefonts.com";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = "Music Menu: Musica electronica sin COPYRIGHT";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = "Music Gameplay: Kevin MacLeod";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = "Sounds: theallsounds.com";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorNoSelected);

			text = "PRESS ENTER FOR BACK TO MENU";
			posY = game::screenHeight * 7 / 8;
			fonts::drawCenterOption(text, posX, posY, fonts::optionColorSelected);
		}

		void deInit()
		{
		}
	}
}