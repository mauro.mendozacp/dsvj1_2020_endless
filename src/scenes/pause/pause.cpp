#include "pause.h"
#include "game/game.h"

namespace endless
{
	namespace pause
	{
		enum class PAUSE_MENU
		{
			CONTINUE = 1,
			GO_BACK
		};

		PAUSE_MENU pauseMenu;

		static PAUSE_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<PAUSE_MENU>(static_cast<int>(pauseMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<PAUSE_MENU>(static_cast<int>(pauseMenu) + 1);
			}

			return pauseMenu;
		}

		static bool checkOption(PAUSE_MENU auxOption)
		{
			switch (auxOption)
			{
			case PAUSE_MENU::CONTINUE:
			case PAUSE_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				PAUSE_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					pauseMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (pauseMenu)
				{
				case PAUSE_MENU::CONTINUE:
					game::gameStatus = game::GAME_STATUS::GAMEPLAY;
					deInit();
					break;
				case PAUSE_MENU::GO_BACK:
					gameplay::deInit();
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(PAUSE_MENU option)
		{
			if (option == pauseMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			pauseMenu = PAUSE_MENU::CONTINUE;
		}

		void update()
		{
			moveOption();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			background::draw();

			//Title
			const char* text = "PAUSE";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "CONTINUE";
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(PAUSE_MENU::CONTINUE));

			text = "BACK TO MENU";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(PAUSE_MENU::GO_BACK));
		}

		void deInit()
		{
		}
	}
}