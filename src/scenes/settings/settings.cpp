#include "settings.h"
#include "game/game.h"

namespace endless
{
	namespace settings
	{
		enum class SETTINGS_MENU
		{
			GENERAL = 1,
			CONTROLS,
			GO_BACK
		};

		SETTINGS_MENU settingsMenu;

		static SETTINGS_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<SETTINGS_MENU>(static_cast<int>(settingsMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<SETTINGS_MENU>(static_cast<int>(settingsMenu) + 1);
			}

			return settingsMenu;
		}

		static bool checkOption(SETTINGS_MENU auxOption)
		{
			switch (auxOption)
			{
			case SETTINGS_MENU::GENERAL:
			case SETTINGS_MENU::CONTROLS:
			case SETTINGS_MENU::GO_BACK:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				SETTINGS_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					settingsMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (settingsMenu)
				{
				case SETTINGS_MENU::GENERAL:
					game::changeStatus(game::GAME_STATUS::GENERAL_SETTINGS);
					break;
				case SETTINGS_MENU::CONTROLS:
					game::changeStatus(game::GAME_STATUS::CONTROLS_SETTINGS);
					break;
				case SETTINGS_MENU::GO_BACK:
					game::changeStatus(game::GAME_STATUS::MAIN_MENU);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(SETTINGS_MENU option)
		{
			if (option == settingsMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			settingsMenu = SETTINGS_MENU::GENERAL;
		}

		void update()
		{
			moveOption();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "SETTINGS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "GENERAL";
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(SETTINGS_MENU::GENERAL));

			text = "CONTROLS";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(SETTINGS_MENU::CONTROLS));

			text = "BACK TO MENU";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(SETTINGS_MENU::GO_BACK));
		}

		void deInit()
		{
		}
	}
}