#include "main_menu.h"
#include "game/game.h"

namespace endless
{
	namespace main_menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			SETTINGS,
			CREDITS,
			EXIT
		};

		MAIN_MENU mainMenu;

		static MAIN_MENU getNewOptionMenu()
		{
			if (input::checkMoveUpOption())
			{
				return static_cast<MAIN_MENU>(static_cast<int>(mainMenu) - 1);
			}
			if (input::checkMoveDownOption())
			{
				return static_cast<MAIN_MENU>(static_cast<int>(mainMenu) + 1);
			}

			return mainMenu;
		}

		static bool checkOption(MAIN_MENU auxOption)
		{
			switch (auxOption)
			{
			case MAIN_MENU::PLAY:
			case MAIN_MENU::SETTINGS:
			case MAIN_MENU::CREDITS:
			case MAIN_MENU::EXIT:
				return true;
			default:
				return false;
			}
		}

		static void moveOption()
		{
			if (input::checkMoveUpDownOption())
			{
				MAIN_MENU auxOption = getNewOptionMenu();

				if (checkOption(auxOption))
				{
					mainMenu = auxOption;
					PlaySound(audio::optionSound);
				}
			}
		}

		static void acceptOption()
		{
			if (input::checkAcceptOption())
			{
				switch (mainMenu)
				{
				case MAIN_MENU::PLAY:
					texture::deInitMenu();
					StopMusicStream(audio::menuMusic);
					game::changeStatus(game::GAME_STATUS::GAMEPLAY);
					break;
				case MAIN_MENU::SETTINGS:
					game::changeStatus(game::GAME_STATUS::SETTINGS);
					break;
				case MAIN_MENU::CREDITS:
					game::changeStatus(game::GAME_STATUS::CREDITS);
					break;
				case MAIN_MENU::EXIT:
					texture::deInitMenu();
					game::changeStatus(game::GAME_STATUS::EXIT);
					break;
				default:
					break;
				}
			}
		}

		static Color getColorOption(MAIN_MENU option)
		{
			if (option == mainMenu)
			{
				return fonts::optionColorSelected;
			}

			return fonts::optionColorNoSelected;
		}

		void init()
		{
			mainMenu = MAIN_MENU::PLAY;
			PlayMusicStream(audio::menuMusic);
		}

		void update()
		{
			moveOption();
			acceptOption();
			UpdateMusicStream(audio::menuMusic);
		}

		void draw()
		{
			//Title
			const char* text = "ENDLESS";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;

			fonts::drawCenterTitle(text, posX, posY, fonts::titleColor);

			//Highscore
			if (game::highScore > 0)
			{
				text = TextFormat("HIGHSCORE: %i", game::highScore);
				posY += static_cast<int>(fonts::titleFontSize + (fonts::titleFontSize / 2));
				fonts::drawCenterOption(text, posX, posY, fonts::titleColor);
			}

			//Options
			int spacingTextY = static_cast<int>(fonts::optionFontSize + (fonts::optionFontSize / 2));

			text = "PLAY";
			posY = game::screenHeight / 2;
			fonts::drawCenterOption(text, posX, posY, getColorOption(MAIN_MENU::PLAY));

			text = "SETTINGS";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(MAIN_MENU::SETTINGS));

			text = "CREDITS";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(MAIN_MENU::CREDITS));

			text = "EXIT";
			posY += spacingTextY;
			fonts::drawCenterOption(text, posX, posY, getColorOption(MAIN_MENU::EXIT));
		}

		void deInit()
		{
		}
	}
}