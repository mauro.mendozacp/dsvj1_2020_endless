#include "potion.h"
#include "game/game.h"

namespace endless
{
	namespace potion
	{
		float potionWidth = 60.0f;
		float potionHeight = 60.0f;

		Potion::Potion()
		{
			this->lifes = 0;
		}

		Potion::Potion(Rectangle rec, float speed, int floor, Color color) : pickup::Pickup(rec, speed, floor, color)
		{
			this->lifes = 1;
			this->texture = texture::potion;
		}

		Potion::~Potion()
		{
		}

		void Potion::collisionPlayer()
		{
			PlaySound(audio::lifeUp);
			gameplay::player->setLifes(gameplay::player->getLifes() + lifes);
		}

		void Potion::setLifes(int lifes)
		{
			this->lifes = lifes;
		}

		int Potion::getLifes()
		{
			return this->lifes;
		}
	}
}