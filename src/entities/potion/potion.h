#ifndef HEART_H
#define HEART_H

#include "raylib.h"
#include "entities/pickup/pickup.h"

namespace endless
{
	namespace potion
	{
		extern float potionWidth;
		extern float potionHeight;

		class Potion: public pickup::Pickup
		{
		public:
			Potion();
			Potion(Rectangle rec, float speed, int floor, Color color);
			~Potion();

			void collisionPlayer();

			void setLifes(int lifes);
			int getLifes();
		private:
			int lifes;
		};
	}
}

#endif // !HEART_H