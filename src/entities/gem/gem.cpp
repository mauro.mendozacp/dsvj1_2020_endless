#include "gem.h"
#include "game/game.h"

namespace endless
{
	namespace gem
	{
		float gemWidth = 40;
		float gemHeight = 35;

		const int gemColorLenght = 4;
		Color gemColors[gemColorLenght] = { BLUE, YELLOW, GREEN, ORANGE };
		int gemPoints[gemColorLenght] = { 10, 15, 25, 50 };

		Gem::Gem()
		{
			this->points = 0;
		}

		Gem::Gem(Rectangle rec, float speed, int floor, Color color): pickup::Pickup(rec, speed, floor, color)
		{
			this->points = getPointsForColor();
			this->texture = getTextureForColor();
		}

		Gem::~Gem()
		{
		}

		void Gem::collisionPlayer()
		{
			PlaySound(audio::grabGem);
			gameplay::player->setScore(gameplay::player->getScore() + points);
		}

		void Gem::setPoints(int points)
		{
			this->points = points;
		}

		int Gem::getPoints()
		{
			return this->points;
		}

		Texture2D Gem::getTextureForColor()
		{
			if (ColorToInt(color) == ColorToInt(gemColors[0]))
			{
				return texture::gemBlue;
			}
			if (ColorToInt(color) == ColorToInt(gemColors[1]))
			{
				return texture::gemYellow;
			}
			if (ColorToInt(color) == ColorToInt(gemColors[2]))
			{
				return texture::gemGreen;
			}
			if (ColorToInt(color) == ColorToInt(gemColors[3]))
			{
				return texture::gemOrange;
			}

			return Texture2D();
		}

		int Gem::getPointsForColor()
		{
			for (int i = 0; i < gemColorLenght; i++)
			{
				if (ColorToInt(color) == ColorToInt(gemColors[i]))
				{
					return gemPoints[i];
				}
			}

			return 0;
		}
	}
}