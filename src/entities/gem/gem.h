#ifndef GEM_H
#define GEM_H

#include "raylib.h"
#include "entities/pickup/pickup.h"

namespace endless
{
	namespace gem
	{
		extern float gemWidth;
		extern float gemHeight;

		extern const int gemColorLenght;
		extern Color gemColors[];

		class Gem: public pickup::Pickup
		{
		public:
			Gem();
			Gem(Rectangle rec, float speed, int floor, Color color);
			~Gem();

			void collisionPlayer();

			void setPoints(int points);
			int getPoints();
		private:
			int points;

			Texture2D getTextureForColor();
			int getPointsForColor();
		};
	}
}

#endif // !GEM_H