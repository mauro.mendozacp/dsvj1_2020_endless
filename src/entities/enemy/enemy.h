#ifndef ENEMY_H
#define ENEMY_H

#include "raylib.h"
#include "entities/obstacle/obstacle.h"
#include "entities/animation/animation.h"

namespace endless
{
	namespace enemy
	{
		extern float enemyWidth;
		extern float enemyHeight;

		class Enemy: public obstacle::Obstacle
		{
		public:
			Enemy();
			Enemy(Rectangle rec, Color color, float speed, int floor);
			~Enemy();

			void update();
			void show();

			void setIsDead(bool isDead);
			bool getIsDead();
			void setAnimationState(animation::ANIMATION_STATE animateState);
			animation::ANIMATION_STATE getAnimationState();
			void setAnimation(animation::Animation animation);
			animation::Animation getAnimation();

		private:
			bool isDead;
			animation::ANIMATION_STATE animateState;
			animation::Animation animation;

			animation::Animation getAnimationForState(animation::ANIMATION_STATE anim);
			void changeAnimation(animation::ANIMATION_STATE anim);
		};
	}
}

#endif // !ENEMY_H