#include "enemy.h"
#include "game/game.h"

namespace endless
{
	namespace enemy
	{
		float enemyWidth = 80.0f;
		float enemyHeight = 120.0f;

		Enemy::Enemy()
		{
			this->animateState = animation::ANIMATION_STATE();
			this->animation = animation::Animation();
			this->isDead = false;
		}

		Enemy::Enemy(Rectangle rec, Color color, float speed, int floor) : obstacle::Obstacle(rec, color, speed, floor)
		{
			this->animateState = animation::ANIMATION_STATE::RUN;
			this->animation = getAnimationForState(animation::ANIMATION_STATE::RUN);
			this->isDead = false;
		}

		Enemy ::~Enemy()
		{
		}

		void Enemy::update()
		{
			move();

			if (gameplay::player->getFloor() == floor)
			{
				if (checkCollisionPlayer(gameplay::player->getRec()))
				{
					if (gameplay::player->getIsAttack())
					{
						isDead = true;
						changeAnimation(animation::ANIMATION_STATE::DEAD);
					}
				}
			}

			if (!isDead)
			{
				collisionPlayer();
			}

			animation.update();
		}

		void Enemy::show()
		{
			animation.draw(getPosition(), getColor());

#if DEBUG
			DrawRectangleLines(static_cast<int>(getRec().x), static_cast<int>(getRec().y), static_cast<int>(getRec().width), static_cast<int>(getRec().height), GREEN);
#endif // DEBUG
		}

		void Enemy::setIsDead(bool isDead)
		{
			this->isDead = isDead;
		}

		bool Enemy::getIsDead()
		{
			return this->isDead;
		}

		void Enemy::setAnimationState(animation::ANIMATION_STATE animateState)
		{
			this->animateState = animateState;
		}

		animation::ANIMATION_STATE Enemy::getAnimationState()
		{
			return this->animateState;
		}

		void Enemy::setAnimation(animation::Animation animation)
		{
			this->animation = animation;
		}

		animation::Animation Enemy::getAnimation()
		{
			return this->animation;
		}

		animation::Animation Enemy::getAnimationForState(animation::ANIMATION_STATE anim)
		{
			animation::Animation auxAnim = animation::Animation();
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = Rectangle();
			int auxFrames = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (anim)
			{
			case animation::ANIMATION_STATE::RUN:
				auxTexture = texture::enemyRun;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::enemyRun.width) / 6, static_cast<float>(texture::enemyRun.height / 2) };
				auxFrames = 12;
				auxSpeed = 950.0f;
				auxIsLoop = true;

				break;
			case animation::ANIMATION_STATE::DEAD:
				auxTexture = texture::enemyDead;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::enemyDead.width) / 6, static_cast<float>(texture::enemyDead.height / 3) };
				auxFrames = 18;
				auxSpeed = 1750.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxAnim.setTexture(auxTexture);
			auxAnim.setFrameRec(auxPlayerRec);
			auxAnim.setFrames(auxFrames);
			auxAnim.setSpeed(auxSpeed);
			auxAnim.setIsLoop(auxIsLoop);

			return auxAnim;
		}

		void Enemy::changeAnimation(animation::ANIMATION_STATE anim)
		{
			animateState = anim;
			animation = getAnimationForState(anim);
		}
	}
}