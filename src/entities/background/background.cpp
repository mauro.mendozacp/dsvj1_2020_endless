#include "background.h"
#include "game/game.h"

namespace endless
{
	namespace background
	{
		void init()
		{
			Color bgColor = WHITE;

			float fPosX1 = -static_cast<float>((texture::floor.width / 3));
			float fPosX2 = -static_cast<float>((texture::floor.width * 2 / 3));;
			float fPosX3 = 0.0f;

			gameplay::floor[0] = new background::Background(texture::floor, fPosX1, gameplay::floorPositionY[0], gameplay::speedX, bgColor);
			gameplay::floor[1] = new background::Background(texture::floor, fPosX2, gameplay::floorPositionY[1], gameplay::speedX, bgColor);
			gameplay::floor[2] = new background::Background(texture::floor, fPosX3, gameplay::floorPositionY[2], gameplay::speedX, bgColor);

			gameplay::parallax[0] = new background::Background(texture::parallax4, (gameplay::speedX / 100 * 50), bgColor);
			gameplay::parallax[1] = new background::Background(texture::parallax3, (gameplay::speedX / 100 * 70), bgColor);
			gameplay::parallax[2] = new background::Background(texture::parallax2, (gameplay::speedX / 100 * 85), bgColor);
			gameplay::parallax[3] = new background::Background(texture::parallax1, gameplay::speedX, bgColor);
		}

		void update()
		{
			float frameTime = GetFrameTime();

			for (int i = 0; i < gameplay::parallaxLenght; i++)
			{
				gameplay::parallax[i]->move(frameTime);
			}

			for (int i = 0; i < gameplay::floorLenght; i++)
			{
				gameplay::floor[i]->move(frameTime);
			}
		}

		void draw()
		{
			for (int i = 0; i < gameplay::parallaxLenght; i++)
			{
				gameplay::parallax[i]->draw();
			}

			for (int i = 0; i < gameplay::floorLenght; i++)
			{
				gameplay::floor[i]->draw();
			}
		}

		void deInit()
		{
			for (int i = 0; i < gameplay::parallaxLenght; i++)
			{
				delete gameplay::parallax[i];
			}

			for (int i = 0; i < gameplay::floorLenght; i++)
			{
				delete gameplay::floor[i];
			}
		}

		Background::Background()
		{
			for (int i = 0; i < bgLenght; i++)
			{
				this->texture[i] = Texture2D();
				this->position[i] = Vector2();
			}

			this->speed = 0.0f;
			this->color = Color();
		}

		Background::Background(Texture2D texture, float speed, Color color)
		{
			for (int i = 0; i < bgLenght; i++)
			{
				this->texture[i] = texture;
				this->position[i].x = static_cast<float>(texture.width * i);
				this->position[i].y = 0;
			}

			this->speed = speed;
			this->color = color;
		}

		Background::Background(Texture2D texture, float positionX, float positionY, float speed, Color color)
		{
			for (int i = 0; i < bgLenght; i++)
			{
				this->texture[i] = texture;
				this->position[i].x = static_cast<float>(texture.width * i) + positionX;
				this->position[i].y = positionY;
			}

			this->speed = speed;
			this->color = color;
		}

		Background::~Background()
		{
		}

		void Background::move(float frameTime)
		{
			float posX = speed * frameTime;

			for (int i = 0; i < bgLenght; i++)
			{
				position[i].x -= posX;

				if (position[i].x <= -texture[i].width)
				{
					position[i].x = static_cast<float>(texture[i].width * (bgLenght - 1));
				}
			}
		}

		void Background::draw()
		{
			for (int i = 0; i < bgLenght; i++)
			{
				DrawTexture(texture[i], static_cast<int>(position[i].x), static_cast<int>(position[i].y), color);
#if DEBUG
				DrawRectangleLines(static_cast<int>(position[i].x), static_cast<int>(position[i].y), static_cast<int>(texture[i].width), static_cast<int>(texture[i].height), GREEN);
#endif // DEBUG
			}
		}

		void Background::setSpeed(float speed)
		{
			this->speed = speed;
		}

		float Background::getSpeed()
		{
			return this->speed;
		}

		void Background::setColor(Color color)
		{
			this->color = color;
		}

		Color Background::getColor()
		{
			return this->color;
		}
	}
}