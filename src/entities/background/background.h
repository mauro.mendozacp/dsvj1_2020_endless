#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "raylib.h"

namespace endless
{
	namespace background
	{
		void init();
		void update();
		void draw();
		void deInit();

		const int bgLenght = 3;
		class Background
		{
		public:
			Background();
			Background(Texture2D texture, float speed, Color color);
			Background(Texture2D texture, float positionX, float positionY, float speed, Color color);
			~Background();

			void move(float posX);
			void draw();

			void setSpeed(float speed);
			float getSpeed();
			void setColor(Color color);
			Color getColor();

		private:
			Texture2D texture[bgLenght];
			Vector2 position[bgLenght];
			float speed;
			Color color;
		};
	}
}

#endif // !BACKGROUND_H