#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "entities/animation/animation.h"

namespace endless
{
	namespace player
	{
		extern float playerWidth;
		extern float playerHeight;

		void init();
		void update();
		void draw();
		void deInit();

		class Player
		{
		public:
			Player();
			Player(Rectangle rec, Color color, float speed, int lifes, int floor);
			~Player();

			void attack();
			void jump();
			void hitted();
			void changeFloor();
			void updateAnimation();
			void changeAnimation(animation::ANIMATION_STATE anim);
			void show();
			void setSizeX(float width);

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			void setColor(Color color);
			Color getColor();
			void setSpeed(float speed);
			float getSpeed();
			void setLifes(int lifes);
			int getLifes();
			void setScore(int score);
			int getScore();
			void setFloor(int floor);
			int getFloor();
			void setIsJump(bool isJump);
			bool getIsJump();
			void setIsHitted(bool isHitted);
			bool getIsHitted();
			void setIsAttack(bool isAttack);
			bool getIsAttack();
			void setIsChangeFloor(bool isChangeFloor);
			bool getIsChangeFloor();
			void setAnimationState(animation::ANIMATION_STATE animateState);
			animation::ANIMATION_STATE getAnimationState();
			void setAnimation(animation::Animation animation);
			animation::Animation getAnimation();

		private:
			Rectangle rec;
			Color color;
			float speed;
			int lifes;
			int score;
			int floor;
			bool isJump;
			bool isFall;
			bool isHitted;
			bool isAttack;
			bool isChangeFloor;

			animation::ANIMATION_STATE animateState;
			animation::Animation animation;

			animation::Animation getAnimationForState(animation::ANIMATION_STATE anim);
		};
	}
}

#endif // PLAYER_H