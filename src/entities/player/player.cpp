#include "player.h"
#include "game/game.h"

namespace endless
{
	namespace player
	{
		const float gravity = 9.8f;
		float playerWidth = 80.0f;
		float playerHeight = 120.0f;
		int auxFloor = 0;
		float timer;
		const int maxLifes = 5;

		void init()
		{
			Color cPlayer = WHITE;
			float speedP = 25.0f;
			int startLifes = 3;
			timer = 0;

			Rectangle recPlayer;
			recPlayer.x = static_cast<float>(game::screenWidth / 8);
			recPlayer.y = static_cast<float>(gameplay::floorPositionY[0] - playerHeight);
			recPlayer.width = playerWidth * 3/ 4;
			recPlayer.height = playerHeight;

			gameplay::player = new Player(recPlayer, cPlayer, speedP, startLifes, 1);
		}

		void update()
		{
			gameplay::player->attack();
			gameplay::player->hitted();
			gameplay::player->jump();
			gameplay::player->changeFloor();
			gameplay::player->updateAnimation();
		}

		void draw()
		{
			gameplay::player->show();
		}

		void deInit()
		{
			if (gameplay::player != NULL)
			{
				delete gameplay::player;
			}
		}

		Player::Player()
		{
			this->rec = Rectangle();
			this->color = Color();
			this->speed = 0.0f;
			this->lifes = 0;
			this->score = 0;
			this->floor = 0;
			this->isJump = false;
			this->isFall = false;
			this->isHitted = false;
			this->isAttack = false;
			this->isChangeFloor = false;

			this->animateState = animation::ANIMATION_STATE();
			this->animation = animation::Animation();
		}

		Player::Player(Rectangle rec, Color color, float speed, int lifes, int floor)
		{
			this->rec = rec;
			this->color = color;
			this->speed = speed;
			this->lifes = lifes;
			this->score = 0;
			this->floor = floor;
			this->isJump = false;
			this->isFall = false;
			this->isHitted = false;
			this->isAttack = false;
			this->isChangeFloor = false;

			this->animateState = animation::ANIMATION_STATE::IDLE;
			this->animation = getAnimationForState(animation::ANIMATION_STATE::IDLE);
		}

		Player::~Player()
		{
		}

		void Player::attack()
		{
			if (!isHitted && !isJump && !isChangeFloor)
			{
				if (input::checkAttack() && !isAttack)
				{
					PlaySound(audio::attack);
					isAttack = true;
					changeAnimation(animation::ANIMATION_STATE::ATTACK);
					rec.width = playerWidth;
				}
			}

			if (isAttack)
			{
				if (animation.getIsFinish())
				{
					isAttack = false;
					changeAnimation(animation::ANIMATION_STATE::RUN);
					rec.width = playerWidth * 3 / 4;
				}
			}
		}

		void Player::jump()
		{
			if (!isHitted && !isAttack && !isChangeFloor)
			{
				if (input::checkJump() && !isJump)
				{
					isJump = true;
					changeAnimation(animation::ANIMATION_STATE::JUMP);
				}
			}

			if (isJump)
			{
				if (!isFall && ((rec.y + rec.height) > (gameplay::floorPositionY[floor - 1] - gameplay::floorHeight)))
				{
					rec.y -= speed * gravity * GetFrameTime();
				}
				else
				{
					isFall = true;
				}

				if (isFall)
				{
					rec.y += speed * gravity * GetFrameTime();
				}

				if ((rec.y + rec.height) >= gameplay::floorPositionY[floor - 1])
				{
					isJump = false;
					isFall = false;
					rec.y = gameplay::floorPositionY[floor - 1] - rec.height;
					
					if (!isHitted)
					{
						changeAnimation(animation::ANIMATION_STATE::RUN);
					}
				}
			}
		}

		void Player::hitted()
		{
			if (isHitted)
			{
				timer += GetFrameTime();
				color = RED;

				if (animation.getIsFinish())
				{
					timer = 0.0f;
					isHitted = false;
					isAttack = false;
					color = WHITE;
					changeAnimation(animation::ANIMATION_STATE::RUN);
				}
			}
		}

		void Player::changeFloor()
		{
			bool finish = false;

			if (!isJump && !isHitted && !isAttack && !isChangeFloor)
			{
				if (input::checkMoveUpDownPlayer())
				{
					auxFloor = 0;

					if (input::checkMoveUpPlayer())
					{
						auxFloor = floor - 1;
					}
					if (input::checkMoveDownPlayer())
					{
						auxFloor = floor + 1;
					}

					if (auxFloor >= 1 && auxFloor <= gameplay::floorLenght)
					{
						isChangeFloor = true;
						changeAnimation(animation::ANIMATION_STATE::CHANGE_FLOOR);
					}
				}
			}

			if (isChangeFloor)
			{
				if (floor > auxFloor)
				{
					rec.y -= speed * gravity * GetFrameTime();

					if ((rec.y + rec.height) < (gameplay::floorPositionY[floor - 1] - gameplay::floorHeight))
					{
						finish = true;
					}
				}
				else
				{
					rec.y += speed * gravity * GetFrameTime();

					if ((rec.y + rec.height) > (gameplay::floorPositionY[floor - 1] + gameplay::floorHeight))
					{
						finish = true;
					}
				}

				if (finish)
				{
					isChangeFloor = false;
					floor = auxFloor;
					rec.y = gameplay::floorPositionY[auxFloor - 1] - playerHeight;

					if (!isHitted)
					{
						changeAnimation(animation::ANIMATION_STATE::RUN);
					}
				}
			}
		}

		void Player::updateAnimation()
		{
			animation.update();
		}

		void Player::changeAnimation(animation::ANIMATION_STATE anim)
		{
			animateState = anim;
			animation = getAnimationForState(anim);
		}

		void Player::show()
		{
			animation.draw(getPosition(), color);

#if DEBUG
			DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height), GREEN);
#endif // DEBUG
		}

		void Player::setSizeX(float width)
		{
			rec.width = width;
		}

		void Player::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Player::getRec()
		{
			return this->rec;
		}

		void Player::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Vector2 Player::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void Player::setColor(Color color)
		{
			this->color = color;
		}

		Color Player::getColor()
		{
			return this->color;
		}

		void Player::setSpeed(float speed)
		{
			this->speed;
		}

		float Player::getSpeed()
		{
			return this->speed;
		}

		void Player::setLifes(int lifes)
		{
			if (lifes <= maxLifes)
			{
				this->lifes = lifes;
			}
		}

		int Player::getLifes()
		{
			return this->lifes;
		}

		void Player::setScore(int score)
		{
			this->score = score;
		}

		int Player::getScore()
		{
			return this->score;
		}

		void Player::setFloor(int floor)
		{
			this->floor = floor;
		}

		int Player::getFloor()
		{
			return this->floor;
		}

		void Player::setIsJump(bool isJump)
		{
			this->isJump = isJump;
		}

		bool Player::getIsJump()
		{
			return this->isJump;
		}

		void Player::setIsHitted(bool hitted)
		{
			this->isHitted = hitted;
		}

		bool Player::getIsHitted()
		{
			return this->isHitted;
		}

		void Player::setIsAttack(bool isAttack)
		{
			this->isAttack = isAttack;
		}

		bool Player::getIsAttack()
		{
			return this->isAttack;
		}

		void Player::setIsChangeFloor(bool isChangeFloor)
		{
			this->isChangeFloor = isChangeFloor;
		}

		bool Player::getIsChangeFloor()
		{
			return this->isChangeFloor;
		}

		void Player::setAnimationState(animation::ANIMATION_STATE animateState)
		{
			this->animateState = animateState;
		}

		animation::ANIMATION_STATE Player::getAnimationState()
		{
			return this->animateState;
		}

		void Player::setAnimation(animation::Animation animation)
		{
			this->animation = animation;
		}

		animation::Animation Player::getAnimation()
		{
			return this->animation;
		}

		animation::Animation Player::getAnimationForState(animation::ANIMATION_STATE anim)
		{
			animation::Animation auxAnim = animation::Animation();
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = Rectangle();
			int auxFrames = 0;
			float auxSpeed = 0.0f;
			bool auxIsLoop = false;

			switch (anim)
			{
			case animation::ANIMATION_STATE::IDLE:
				auxTexture = texture::playerIdle;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerIdle.width) / 6, static_cast<float>(texture::playerIdle.height / 2) };
				auxFrames = 12;
				auxSpeed = 950.0f;
				auxIsLoop = true;

				break;
			case animation::ANIMATION_STATE::RUN:
				auxTexture = texture::playerRun;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerRun.width) / 6, static_cast<float>(texture::playerRun.height / 3) };
				auxFrames = 18;
				auxSpeed = 950.0f;
				auxIsLoop = true;

				break;
			case animation::ANIMATION_STATE::JUMP:
			case animation::ANIMATION_STATE::CHANGE_FLOOR:
				auxTexture = texture::playerJump;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerJump.width) / 6, static_cast<float>(texture::playerJump.height) };
				auxFrames = 6;
				auxSpeed = 950.0f;
				auxIsLoop = true;

				break;
			case animation::ANIMATION_STATE::HITTED:
				auxTexture = texture::playerHitted;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerHitted.width) / 6, static_cast<float>(texture::playerHitted.height) / 2 };
				auxFrames = 6;
				auxSpeed = 950.0f;
				auxIsLoop = false;

				break;
			case animation::ANIMATION_STATE::DEAD:
				auxTexture = texture::playerDead;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerRun.width) / 6, static_cast<float>(texture::playerRun.height / 3) };
				auxFrames = 18;
				auxSpeed = 650.0f;
				auxIsLoop = false;

				break;
			case animation::ANIMATION_STATE::ATTACK:
				auxTexture = texture::playerAttack;
				auxPlayerRec = { 0.0f, 0.0f, static_cast<float>(texture::playerAttack.width) / 6, static_cast<float>(texture::playerAttack.height) / 2 };
				auxFrames = 12;
				auxSpeed = 1750.0f;
				auxIsLoop = false;

				break;
			default:
				break;
			}

			auxAnim.setTexture(auxTexture);
			auxAnim.setFrameRec(auxPlayerRec);
			auxAnim.setFrames(auxFrames);
			auxAnim.setSpeed(auxSpeed);
			auxAnim.setIsLoop(auxIsLoop);

			return auxAnim;
		}
	}
}