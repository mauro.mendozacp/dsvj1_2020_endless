#ifndef PICKUP_H
#define PICKUP_H

#include "raylib.h"

namespace endless
{
	namespace pickup
	{
		void init();
		void create(int floor);
		void update();
		void draw();
		void deInit();

		class Pickup
		{
		public:
			Pickup();
			Pickup(Rectangle rec, float speed, int floor, Color color);
			~Pickup();

			void move();
			bool checkCollisionLimit();
			bool checkCollisionPlayer(Rectangle playerRec);
			virtual void collisionPlayer() = 0;
			void show();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setSpeed(float speed);
			float getSpeed();
			void setFloor(int floor);
			int getFloor();
			void setColor(Color color);
			Color getColor();

		protected:
			Rectangle rec;
			Texture2D texture;
			float speed;
			int floor;
			Color color;
		};
	}
}

#endif // !PICKUP