#include "pickup.h"
#include "game/game.h"

namespace endless
{
	namespace pickup
	{
		const int putPickupForSec = 2;
		bool cooldown;

		void init()
		{
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				gameplay::pickups[i] = NULL;
			}
			cooldown = false;
		}

		void create(int iFloor)
		{
			//Put Pickup
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				if (gameplay::pickups[i] == NULL)
				{
					Rectangle auxPickup = Rectangle();

					if (GetRandomValue(1, 100) < 95)
					{
						//Start gem pickup
						Color auxColor = gem::gemColors[GetRandomValue(0, gem::gemColorLenght - 1)];

						auxPickup.x = static_cast<float>(game::screenWidth);
						auxPickup.y = gameplay::floorPositionY[iFloor] - (gem::gemHeight * 5 / 4);
						auxPickup.width = gem::gemWidth;
						auxPickup.height = gem::gemHeight;

						gameplay::pickups[i] = new gem::Gem(auxPickup, gameplay::speedX, iFloor + 1, auxColor);
					}
					else
					{
						//Start potion pickup
						auxPickup.x = static_cast<float>(game::screenWidth);
						auxPickup.y = gameplay::floorPositionY[iFloor] - (potion::potionHeight * 5 / 4);
						auxPickup.width = potion::potionWidth;
						auxPickup.height = potion::potionHeight;

						gameplay::pickups[i] = new potion::Potion(auxPickup, gameplay::speedX, iFloor + 1, WHITE);
					}

					break;
				}
			}
		}

		void update()
		{
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				if (gameplay::pickups[i] != NULL)
				{
					gameplay::pickups[i]->move();

					//Collision Limit
					if (gameplay::pickups[i]->checkCollisionLimit())
					{
						gameplay::pickups[i] = NULL;
					}
					else
					{
						//Collision Player
						if (gameplay::player->getFloor() == gameplay::pickups[i]->getFloor())
						{
							if (gameplay::pickups[i]->checkCollisionPlayer(gameplay::player->getRec()))
							{
								gameplay::pickups[i]->collisionPlayer();
								gameplay::pickups[i] = NULL;
							}
						}
					}
				}
			}
		}

		void draw()
		{
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				if (gameplay::pickups[i] != NULL)
				{
					gameplay::pickups[i]->show();
				}
			}
		}

		void deInit()
		{
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				if (gameplay::pickups[i] != NULL)
				{
					delete gameplay::pickups[i];
				}
			}
		}

		Pickup::Pickup()
		{
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->speed = 0.0f;
			this->floor = 0;
			this->color = Color();
		}

		Pickup::Pickup(Rectangle rec, float speed, int floor, Color color)
		{
			this->rec = rec;
			this->texture = Texture2D();
			this->speed = speed;
			this->color = color;
			this->floor = floor;
		}

		Pickup::~Pickup()
		{
		}

		void Pickup::move()
		{
			rec.x -= speed * GetFrameTime();
		}

		bool Pickup::checkCollisionLimit()
		{
			if (rec.x + rec.width <= 0)
			{
				return true;
			}

			return false;
		}

		bool Pickup::checkCollisionPlayer(Rectangle playerRec)
		{
			if (CheckCollisionRecs(rec, playerRec))
			{
				return true;
			}

			return false;
		}

		void Pickup::show()
		{
			DrawTexture(texture, static_cast<int>(rec.x), static_cast<int>(rec.y), LIGHTGRAY);

#if DEBUG
			DrawRectangleLines(static_cast<int>(rec.x), static_cast<int>(rec.y), static_cast<int>(rec.width), static_cast<int>(rec.height), GREEN);
#endif // DEBUG
		}

		void Pickup::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Pickup::getRec()
		{
			return this->rec;
		}

		void Pickup::setPosition(Vector2 position)
		{
			rec.x = position.x;
			rec.y = position.y;
		}

		Vector2 Pickup::getPosition()
		{
			return { rec.x, rec.y };
		}

		void Pickup::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Pickup::getTexture()
		{
			return this->texture;
		}

		void Pickup::setSpeed(float speed)
		{
			this->speed = speed;
		}

		float Pickup::getSpeed()
		{
			return this->speed;
		}

		void Pickup::setFloor(int floor)
		{
			this->floor = floor;
		}

		int Pickup::getFloor()
		{
			return this->floor;
		}

		void Pickup::setColor(Color color)
		{
			this->color = color;
		}

		Color Pickup::getColor()
		{
			return this->color;
		}
	}
}