#include "obstacle.h"
#include "game/game.h"

namespace endless
{
	namespace obstacle
	{
		const int putPickupForSec = 2;
		bool cooldown;

		void init()
		{
			for (int i = 0; i < gameplay::obstacleLenght; i++)
			{
				gameplay::obstacles[i] = NULL;
			}
		}

		void create(int iFloor)
		{
			for (int i = 0; i < gameplay::obstacleLenght; i++)
			{
				if (gameplay::obstacles[i] == NULL)
				{
					Texture2D auxTexture = Texture2D();
					Rectangle auxRec = Rectangle();

					if (GetRandomValue(1, 4) < 4)
					{
						if (GetRandomValue(0, 1) == 0)
						{
							auxTexture = texture::spikes;
							auxRec.x = static_cast<float>(game::screenWidth);
							auxRec.y = gameplay::floorPositionY[iFloor] - trap::spikesHeight;
							auxRec.width = trap::spikesWidth;
							auxRec.height = trap::spikesHeight;
						}
						else
						{
							auxTexture = texture::rock;
							auxRec.x = static_cast<float>(game::screenWidth);
							auxRec.y = gameplay::floorPositionY[iFloor] - trap::rockHeight;
							auxRec.width = trap::rockWidth;
							auxRec.height = trap::rockHeight;
						}

						gameplay::obstacles[i] = new trap::Trap(auxRec, WHITE, gameplay::speedX, iFloor +1, auxTexture);
					}
					else
					{
						auxRec.x = static_cast<float>(game::screenWidth);
						auxRec.y = gameplay::floorPositionY[iFloor] - enemy::enemyHeight;
						auxRec.width = enemy::enemyWidth;
						auxRec.height = enemy::enemyHeight;

						gameplay::obstacles[i] = new enemy::Enemy(auxRec, WHITE, gameplay::speedX, iFloor+1);
					}

					break;
				}
			}
		}

		void update()
		{
			for (int i = 0; i < gameplay::obstacleLenght; i++)
			{
				if (gameplay::obstacles[i] != NULL)
				{
					gameplay::obstacles[i]->update();

					//Collision Limit
					if (gameplay::obstacles[i]->checkCollisionLimit())
					{
						gameplay::obstacles[i] = NULL;
					}
				}
			}
		}

		void draw()
		{
			for (int i = 0; i < gameplay::pickupLenght; i++)
			{
				if (gameplay::obstacles[i] != NULL)
				{
					gameplay::obstacles[i]->show();
				}
			}
		}

		void deInit()
		{
			for (int i = 0; i < gameplay::obstacleLenght; i++)
			{
				if (gameplay::obstacles[i] != NULL)
				{
					delete gameplay::obstacles[i];
				}
			}
		}

		Obstacle::Obstacle()
		{
			this->rec = Rectangle();
			this->color = Color();
			this->speed = 0.0f;
			this->floor = 0;
		}

		Obstacle::Obstacle(Rectangle rec, Color color, float speed, int floor)
		{
			this->rec = rec;
			this->color = color;
			this->speed = speed;
			this->floor = floor;
		}

		Obstacle::~Obstacle()
		{
		}

		void Obstacle::move()
		{
			rec.x -= speed * GetFrameTime();
		}

		void Obstacle::collisionPlayer()
		{
			if (!gameplay::player->getIsHitted())
			{
				if (gameplay::player->getFloor() == floor)
				{
					if (checkCollisionPlayer(gameplay::player->getRec()))
					{
						PlaySound(audio::hit);
						gameplay::player->setLifes(gameplay::player->getLifes() - 1);

						if (gameplay::player->getLifes() > 0)
						{
							gameplay::player->setIsHitted(true);
							gameplay::player->changeAnimation(animation::ANIMATION_STATE::HITTED);

							if (gameplay::player->getIsAttack())
							{
								gameplay::player->setIsAttack(false);
								gameplay::player->setSizeX(player::playerWidth * 3 / 4);
							}
						}
						else
						{
							gameplay::player->changeAnimation(animation::ANIMATION_STATE::DEAD);
						}
					}
				}
			}
		}

		bool Obstacle::checkCollisionLimit()
		{
			if (rec.x + rec.width <= 0)
			{
				return true;
			}

			return false;
		}

		bool Obstacle::checkCollisionPlayer(Rectangle playerRec)
		{
			if (CheckCollisionRecs(rec, playerRec))
			{
				return true;
			}

			return false;
		}

		void Obstacle::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Obstacle::getRec()
		{
			return this->rec;
		}

		void Obstacle::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		Vector2 Obstacle::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void Obstacle::setColor(Color color)
		{
			this->color = color;
		}

		Color Obstacle::getColor()
		{
			return this->color;
		}

		void Obstacle::setSpeed(float speed)
		{
			this->speed;
		}

		float Obstacle::getSpeed()
		{
			return this->speed;
		}

		void Obstacle::setFloor(int floor)
		{
			this->floor = floor;
		}

		int Obstacle::getFloor()
		{
			return this->floor;
		}
	}
}