#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "raylib.h"

namespace endless
{
	namespace obstacle
	{
		void init();
		void create(int floor);
		void update();
		void draw();
		void deInit();

		class Obstacle
		{
		public:
			Obstacle();
			Obstacle(Rectangle rec, Color color, float speed, int floor);
			~Obstacle();

			virtual void update() = 0;
			virtual void show() = 0;
			bool checkCollisionLimit();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			void setColor(Color color);
			Color getColor();
			void setSpeed(float speed);
			float getSpeed();
			void setFloor(int floor);
			int getFloor();

		protected:
			Rectangle rec;
			Color color;
			float speed;
			int floor;

			void move();
			void collisionPlayer();
			bool checkCollisionPlayer(Rectangle playerRec);
		};
	}
}

#endif // !OBSTACLE_H