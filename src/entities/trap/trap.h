#ifndef PROP_H
#define PROP_H

#include "raylib.h"
#include "entities/obstacle/obstacle.h"

namespace endless
{
	namespace trap
	{
		extern float spikesWidth;
		extern float spikesHeight;
		extern float rockWidth;
		extern float rockHeight;

		class Trap: public obstacle::Obstacle
		{
		public:
			Trap();
			Trap(Rectangle rec, Color color, float speed, int floor, Texture2D texture);
			~Trap();

			void update();
			void show();

			void setTexture(Texture2D texture);
			Texture2D getTexture();
		private:
			Texture2D texture;
		};
	}
}

#endif // !PROP_H