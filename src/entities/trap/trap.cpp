#include "trap.h"

namespace endless
{
	namespace trap
	{
		float spikesWidth = 80.0f;
		float spikesHeight = 40.0f;
		float rockWidth = 80.0f;
		float rockHeight = 80.0f;

		Trap::Trap()
		{
			this->texture = Texture2D();
		}

		Trap::Trap(Rectangle rec, Color color, float speed, int floor, Texture2D texture): obstacle::Obstacle(rec, color, speed, floor)
		{
			this->texture = texture;
		}

		Trap::~Trap()
		{
		}

		void Trap::update()
		{
			move();
			collisionPlayer();
		}

		void Trap::show()
		{
			DrawTexture(texture, static_cast<int>(getRec().x), static_cast<int>(getRec().y), WHITE);

#if DEBUG
			DrawRectangleLines(static_cast<int>(getRec().x), static_cast<int>(getRec().y), static_cast<int>(getRec().width), static_cast<int>(getRec().height), GREEN);
#endif // DEBUG
		}

		void Trap::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Trap::getTexture()
		{
			return this->texture;
		}
	}
}