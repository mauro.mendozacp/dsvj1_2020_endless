#include "game/game.h"

using namespace endless;

int main(void)
{
    game::runGame();

    return 0;
}