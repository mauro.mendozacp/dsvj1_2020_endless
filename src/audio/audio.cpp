#include "audio.h"

namespace endless
{
	namespace audio
	{
		Music gameplayMusic = LoadMusicStream("res/assets/music/gameplay-soundtrack.mp3");
		Music menuMusic = LoadMusicStream("res/assets/music/menu-soundtrack.mp3");

		Sound optionSound = LoadSound("res/assets/sounds/menu-selection.ogg");
		Sound grabGem = LoadSound("res/assets/sounds/grab_gem.ogg");
		Sound lifeUp = LoadSound("res/assets/sounds/life_up.ogg");
		Sound hit = LoadSound("res/assets/sounds/hit.ogg");
		Sound attack = LoadSound("res/assets/sounds/attack.ogg");

		float musicVolume = 0.6f;
		float sfxVolume = 0.8f;

		void init()
		{
			InitAudioDevice();
			setMusicVolume(musicVolume);
			setSfxVolume(sfxVolume);
		}

		void deInit()
		{
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);
			UnloadSound(optionSound);
			UnloadSound(grabGem);
			UnloadSound(lifeUp);
			UnloadSound(hit);
			UnloadSound(attack);
			CloseAudioDevice();
		}

		void setMusicVolume(float volume)
		{
			musicVolume = volume;
			SetMusicVolume(gameplayMusic, volume);
			SetMusicVolume(menuMusic, volume);
		}

		void setSfxVolume(float volume)
		{
			sfxVolume = volume;
			SetSoundVolume(optionSound, volume);
			SetSoundVolume(grabGem, volume);
			SetSoundVolume(lifeUp, volume);
			SetSoundVolume(hit, volume);
			SetSoundVolume(attack, volume);
		}
	}
}