#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace endless
{
	namespace audio
	{
		extern Music gameplayMusic;
		extern Music menuMusic;
		
		extern Sound optionSound;
		extern Sound grabGem;
		extern Sound lifeUp;
		extern Sound hit;
		extern Sound attack;

		extern float musicVolume;
		extern float sfxVolume;

		void init();
		void deInit();

		void setMusicVolume(float volume);
		void setSfxVolume(float volume);
	}
}

#endif // !AUDIO_H