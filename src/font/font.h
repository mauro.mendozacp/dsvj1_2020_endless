#ifndef FONTS_H
#define FONTS_H

#include "raylib.h"

namespace endless
{
	namespace fonts
	{
		extern Font title;
		extern Font option;

		extern int titleFontSize;
		extern int optionFontSize;
		extern int valueFontSize;

		extern float titleSpacing;
		extern float optionSpacing;
		extern float valueSpacing;

		extern Color titleColor;
		extern Color optionColorSelected;
		extern Color optionColorNoSelected;

		void drawCenterTitle(const char* text, int posX, int posY, Color color);
		void drawCenterOption(const char* text, int posX, int posY, Color color);
		void drawCenterValue(const char* text, int posX, int posY, Color color);
		void init();
		void deInit();
	}
}

#endif // !FONT_H