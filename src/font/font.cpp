#include "font.h"

namespace endless
{
	namespace fonts
	{
		Font title;
		Font option;

		int titleFontSize = 45;
		int optionFontSize = 25;
		int valueFontSize = 20;
		
		float titleSpacing = 10.0f;
		float optionSpacing = 5.0f;
		float valueSpacing = 3.0f;

		Color titleColor = WHITE;
		Color optionColorSelected = YELLOW;
		Color optionColorNoSelected = WHITE;

		void drawCenterTitle(const char* text, int posX, int posY, Color color)
		{
			Vector2 pos;
			pos.x = static_cast<float>((posX - (MeasureTextEx(title, text, static_cast<float>(titleFontSize), titleSpacing).x / 2)));
			pos.y = static_cast<float>((posY - (titleFontSize / 2)));
			DrawTextEx(title, text, pos, static_cast<float>(titleFontSize), titleSpacing, color);
		}

		void drawCenterOption(const char* text, int posX, int posY, Color color)
		{
			Vector2 pos;
			pos.x = static_cast<float>((posX - (MeasureTextEx(option, text, static_cast<float>(optionFontSize), optionSpacing).x / 2)));
			pos.y = static_cast<float>((posY - (optionFontSize / 2)));
			DrawTextEx(option, text, pos, static_cast<float>(optionFontSize), optionSpacing, color);
		}

		void drawCenterValue(const char* text, int posX, int posY, Color color)
		{
			Vector2 pos;
			pos.x = static_cast<float>((posX - (MeasureTextEx(option, text, static_cast<float>(valueFontSize), valueSpacing).x / 2)));
			pos.y = static_cast<float>((posY - (valueFontSize / 2)));
			DrawTextEx(option, text, pos, static_cast<float>(valueFontSize), valueSpacing, color);
		}

		void init()
		{
			option = LoadFont("res/assets/fonts/option.ttf");
			title = LoadFont("res/assets/fonts/title.ttf");
		}

		void deInit()
		{
			UnloadFont(option);
			UnloadFont(title);
		}
	}
}