#include "game.h"

namespace endless
{
	namespace game
	{
		GAME_STATUS gameStatus;
		const char* titleGame = "ENDLESS";

		int screenWidth = 840;
		int screenHeight = 750;
		const int frames = 60;
		int highScore = 0;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, titleGame);

			audio::init();
			fonts::init();
			texture::initMenu();

			#if DEBUG
				audio::setMusicVolume(0);
			#endif // DEBUG

			SetTargetFPS(frames);

			highScore = LoadStorageValue(STORAGE_DATA::STORAGE_POSITION_HISCORE);

			gameStatus = GAME_STATUS::MAIN_MENU;
			main_menu::init();
		}

		static void update()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::GAMEPLAY:
				gameplay::update();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::update();
				break;
			case GAME_STATUS::SETTINGS:
				settings::update();
				break;
			case GAME_STATUS::CREDITS:
				credits::update();
				break;
			case GAME_STATUS::PAUSE:
				pause::update();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::update();
				break;
			case GAME_STATUS::GENERAL_SETTINGS:
				general_settings::update();
				break;
			case GAME_STATUS::CONTROLS_SETTINGS:
				controls_settings::update();
				break;
			default:
				break;
			}
		}

		static void drawBackground()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
			case GAME_STATUS::SETTINGS:
			case GAME_STATUS::GENERAL_SETTINGS:
			case GAME_STATUS::CONTROLS_SETTINGS:
			case GAME_STATUS::CREDITS:
				DrawTexture(texture::backgroundMenu, 0, 0, WHITE);
				break;
			case GAME_STATUS::GAMEOVER:
				DrawTexture(texture::backgroundGameover, 0, 0, WHITE);
				break;
			case GAME_STATUS::GAMEPLAY:
			case GAME_STATUS::PAUSE:
				DrawTexture(texture::backgroundGameplay, 0, 0, WHITE);
				break;
			default:
				break;
			}
		}

		static void deInitBackgroundMenu()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
			case GAME_STATUS::SETTINGS:
			case GAME_STATUS::GENERAL_SETTINGS:
			case GAME_STATUS::CONTROLS_SETTINGS:
			case GAME_STATUS::CREDITS:
				texture::deInitMenu();
				break;
			default:
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();

			ClearBackground(BLACK);

			drawBackground();

			switch (gameStatus)
			{
			case GAME_STATUS::GAMEPLAY:
				gameplay::draw();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::draw();
				break;
			case GAME_STATUS::SETTINGS:
				settings::draw();
				break;
			case GAME_STATUS::CREDITS:
				credits::draw();
				break;
			case GAME_STATUS::PAUSE:
				pause::draw();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::draw();
				break;
			case GAME_STATUS::GENERAL_SETTINGS:
				general_settings::draw();
				break;
			case GAME_STATUS::CONTROLS_SETTINGS:
				controls_settings::draw();
				break;
			default:
				break;
			}

			EndDrawing();
		}

		static void deInit()
		{
			CloseWindow();
			audio::deInit();
			fonts::deInit();
			deInitBackgroundMenu();
			changeStatus(GAME_STATUS::EXIT);
		}

		void runGame()
		{
			init();
			while (!WindowShouldClose() && gameStatus != GAME_STATUS::EXIT)
			{
				update();
				draw();
			}
			deInit();
		}

		void changeStatus(GAME_STATUS newGameStatus)
		{
			switch (gameStatus)
			{
			case GAME_STATUS::GAMEPLAY:
				gameplay::deInit();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::deInit();
				break;
			case GAME_STATUS::SETTINGS:
				settings::deInit();
				break;
			case GAME_STATUS::CREDITS:
				credits::deInit();
				break;
			case GAME_STATUS::PAUSE:
				pause::deInit();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::deInit();
				break;
			case GAME_STATUS::GENERAL_SETTINGS:
				general_settings::deInit();
				break;
			case GAME_STATUS::CONTROLS_SETTINGS:
				controls_settings::deInit();
				break;
			case GAME_STATUS::EXIT:
				break;
			default:
				break;
			}

			switch (newGameStatus)
			{
			case GAME_STATUS::GAMEPLAY:
				gameplay::init();
				break;
			case GAME_STATUS::MAIN_MENU:
				main_menu::init();
				break;
			case GAME_STATUS::SETTINGS:
				settings::init();
				break;
			case GAME_STATUS::CREDITS:
				credits::init();
				break;
			case GAME_STATUS::PAUSE:
				pause::init();
				break;
			case GAME_STATUS::GAMEOVER:
				gameover::init();
				break;
			case GAME_STATUS::GENERAL_SETTINGS:
				general_settings::init();
				break;
			case GAME_STATUS::CONTROLS_SETTINGS:
				controls_settings::init();
				break;
			case GAME_STATUS::EXIT:
				break;
			default:
				break;
			}

			gameStatus = newGameStatus;
		}
	}
}