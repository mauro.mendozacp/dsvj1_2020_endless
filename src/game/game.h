#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "font/font.h"
#include "input/input.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/settings/settings.h"
#include "scenes/credits/credits.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/pause/pause.h"
#include "scenes/gameover/gameover.h"
#include "scenes/general_settings/general_settings.h"
#include "scenes/controls_settings/controls_settings.h"

namespace endless
{
	namespace game
	{
		enum STORAGE_DATA
		{
			STORAGE_POSITION_HISCORE = 0
		};

		enum class GAME_STATUS
		{
			GAMEPLAY = 1,
			MAIN_MENU,
			PAUSE,
			GAMEOVER,
			SETTINGS,
			GENERAL_SETTINGS,
			CONTROLS_SETTINGS,
			CREDITS,
			EXIT
		};

		extern GAME_STATUS gameStatus;
		extern const char* titleGame;

		extern int screenWidth;
		extern int screenHeight;
		extern const int frames;
		extern int highScore;

		void runGame();
		void changeStatus(GAME_STATUS newGameStatus);
	}
}

#endif // !GAME_H